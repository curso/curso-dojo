<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html>
<body onLoad="onLoad();">
<head>
<title>Ajax Dojo Example</title>
<script type="text/JavaScript" src="js/dojo/dojo.js"
	djConfig="parseOnLoad: true">	
</script>

<style type="text/css">
	@import "js/dijit/themes/tundra/tundra.css";
	@import "js/dojo/resources/dojo.css";
</style>

<script type="text/JavaScript">
	dojo.require("dijit.form.Button");
	//dojo.require("dojo.io.*");
	//dojo.require("dojo.event.*");
</script>
<script type="text/JavaScript">
	/*
	function onLoad() {
		var buttonObj = document.getElementById("buttonRequestAdmission");
		dojo.event.connect(buttonObj, "onclick", this, "requestAdmission");
	}*/

	require([ "dojo/dom", "dojo/on", "dojo/parser", "dijit/registry",
			"dijit/form/Button", "dojo/domReady!" ], function(dom, on, parser,
			registry) {
		var myClick = function requestAdmission() {
			var REQUEST_URL = "http://localhost:8080/dojo-sample/AdmissionRequest";
			var bindArguments = {
				url : REQUEST_URL,
				error : function(type, data, evt) {
					alert(data);
				},
				load : function(type, data, evt) {
					alert(data);
				},
				mimetype : "text/plain",
				formNode : dom.byId("buttonRequestAdmission")
			};
			//dojo.io.bind(bindArguments);
		}

		parser.parse();
		var buttonObj = dom.byId("buttonRequestAdmission");
		on(buttonObj, "click", myClick);
	});

	
</script>
</head>

<body>
	<form id="formAdmission">
		<h1>Find Student Name</h1>
		<p>
			Enter Roll Number <input type="text" name="roll"> <input
				type="button" id="buttonRequestAdmission" value="Submit" />
		</p>
	</form>
</body>
</html>